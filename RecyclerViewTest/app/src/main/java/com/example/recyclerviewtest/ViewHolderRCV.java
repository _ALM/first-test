package com.example.recyclerviewtest;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class ViewHolderRCV extends RecyclerView.ViewHolder {

    public TextView txtName;
    public RadioButton rbtnTrue, rbtnFalse;
    public ImageView imgView;
    public RadioGroup radioGroup;

    public ViewHolderRCV(@NonNull View itemView) {
        super(itemView);

        txtName = itemView.findViewById(R.id.txtName);
        rbtnFalse = itemView.findViewById(R.id.rbtnFalse);
        rbtnTrue = itemView.findViewById(R.id.rbtnTrue);
        imgView = itemView.findViewById(R.id.imgView);
        radioGroup = itemView.findViewById(R.id.radioGroup);

    }


}
