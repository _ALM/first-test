package com.example.recyclerviewtest;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

public class AdapterRcv extends RecyclerView.Adapter<ViewHolderRCV> {

    private List<PersonModel> _personData;
    private Context context;
    private SharedPreferences _sharedPreferences;

    AdapterRcv(SharedPreferences sharedPreferences) {
        this._sharedPreferences = sharedPreferences;
        _personData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            PersonModel p = new PersonModel();
            p.setName(_sharedPreferences.getString("name-" + i, ""));
            p.setPresent(_sharedPreferences.getString("present-" + i, ""));
            _personData.add(p);
        }
    }

    public List<PersonModel> retrieveData() {
        return _personData;
    }

    @NonNull
    @Override
    public ViewHolderRCV onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        context = viewGroup.getContext();
        return new ViewHolderRCV(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRCV viewHolderRCV, final int i) {

        PersonModel personModel = _personData.get(i);
        viewHolderRCV.txtName.setText(personModel.getName());
        String bool = personModel.getPresent();
        if (bool.equals("true")) {
            viewHolderRCV.rbtnTrue.setChecked(true);
        } else if (bool.equals("false")) {
            viewHolderRCV.rbtnFalse.setChecked(true);
        }

        viewHolderRCV.txtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                _personData.get(i).setName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        viewHolderRCV.rbtnTrue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    _personData.get(i).setPresent("true");
                } else {
                    _personData.get(i).setPresent("");
                }
            }
        });
        viewHolderRCV.rbtnFalse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    _personData.get(i).setPresent("false");
                } else {
                    _personData.get(i).setPresent("");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return _personData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

