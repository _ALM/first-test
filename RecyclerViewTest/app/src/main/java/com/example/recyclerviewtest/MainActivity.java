package com.example.recyclerviewtest;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

//Hello aria

public class MainActivity extends AppCompatActivity {

    Button btnSave;
    RecyclerView rcv;
    AdapterRcv adapterRcv;
    SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rcv = findViewById(R.id.rcv);
        btnSave = findViewById(R.id.btnSave);

        sharedPreferences = getSharedPreferences("Data", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        adapterRcv = new AdapterRcv(sharedPreferences);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<PersonModel> peopleList = adapterRcv.retrieveData();
                for (int i = 0; i < 10; i++) {
                    editor.putString("name-" +i, peopleList.get(i).Name);
                    editor.putString("present-"+i, peopleList.get(i).Present);
                    editor.apply();
                }
                Toast.makeText(getApplicationContext(),"Saved Successfully!",Toast.LENGTH_LONG).show();
            }
        });

        callRcv();

    }

    private void callRcv() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rcv.setLayoutManager(layoutManager);
        rcv.setAdapter(adapterRcv);
    }
}
